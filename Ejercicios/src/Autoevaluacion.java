
import java.util.Scanner;

public class Autoevaluacion {

	public static void main(String[] args) {

		Scanner entrada=new Scanner(System.in);
		int elMayor=0;
		int elMenor=9999;
		int valorEntrada;
		int contadorDatosValidos=0;
		String confirmacion="F";
		
		System.out.println("Introduzca al menos dos números enteros positivos");
		System.out.println("Cuando introduzca el 0 finalizará la entrada de números");
		
		do {
			contadorDatosValidos++;
			valorEntrada=entrada.nextInt();
			
			if (valorEntrada>elMayor) {
				elMayor=valorEntrada;
			}
	
			if (valorEntrada<elMenor && valorEntrada!=0) {
				elMenor=valorEntrada;
			}
			
			if (valorEntrada==0 && contadorDatosValidos <=2) {
				System.out.println("Debe introducir al menos 2 valores");
			}
			
			if (valorEntrada==0 && contadorDatosValidos>2) {
				System.out.println("¿Desea dejar de introducir valores y mostrar resultados?");
				System.out.println("En caso afirmativo pulse T, en caso negativo, pulse cualquier otra tecla");
				System.out.println("En caso negativo, pulse cualquier otra tecla y continue introduciendo valores");
				confirmacion=entrada.next();
			}
			
		}while(!confirmacion.equalsIgnoreCase("T"));
		
		System.out.println("El mayor valor introducido es " + elMayor);
		System.out.println("El menor valor introducido es " + elMenor);
		
		entrada.close();

	}

}
