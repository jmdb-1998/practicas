/**
 * MasFrecuenteInt.java
 * Programa en entorno de pruebas al que al pasar un vectores te informa el numero mas fracuente en este.
 * jmdb - 2021.01.07
 */

package Practica4;

public class MasFrecuenteInt {

	
	private static int [] vector1 = new int[] {4, 1, 1, 4, 2, 3, 4, 4, 1, 2, 4, 9, 3};
	private static boolean ordenado;

	public static void main(String argumentos[]) {

		masFrecuenteInt(vector1);
	}

	public static void masFrecuenteInt(int vector1[]) { 
		
		for (int i = 0; i < vector1.length; i++) {
            if (i + 1 < vector1.length) {
                if (vector1[i] > vector1[i + 1]) {
                    ordenado = false;
                    break;
                }else {
                	ordenado = true;
                }
            }
        }
        if (ordenado) {
            System.out.println("La lista est� ordenada");
        } else {
            System.out.println("La lista est� desordenada");
        }
	}
	
}
