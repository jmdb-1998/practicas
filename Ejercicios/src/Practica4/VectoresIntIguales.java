/**
 * VectoresIntIguales.java
 * Programa en entorno de pruebas al que al pasar dos vectores te informa si estos son iguales o no.
 * jmdb - 2021.01.07
 */

package Practica4;

import java.util.Arrays;

public class VectoresIntIguales {

	private static int [] vector1 = new int[] {2, 4, 5, -4, 7, 2};
	private static int [] vector2 = new int[] {2, 4, 5, -4, 7, 2};

	public static void main(String argumentos[]) {

		vectoresIntIguales(vector1, vector2);
	}

	public static void vectoresIntIguales(int vector1[], int vector2[]) { 

		if (Arrays.equals(vector1, vector2)) {
			System.out.println("Los dos vectores son iguales");
		}else {
			System.out.println("Los dos vectores NO son iguales");
		}

	}

}
