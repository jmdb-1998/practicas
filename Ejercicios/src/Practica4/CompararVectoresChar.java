/**
 * CompararVectoresChar.java
 * Programa en entorno de pruebas al que al pasar dos vectores te informa si estos son iguales, en caso de que si mostrara un 0
 * en caso contrario mostrara un -1.
 * jmdb - 2021.01.07
 */


package Practica4;

import java.util.Arrays;

public class CompararVectoresChar {


	private static int [] vector1 = new int[] {2, 4, 5, -4, 7, 2};
	private static int [] vector2 = new int[] {2, 4, 5, -4, 7, 2};

	public static void main(String argumentos[]) {

		compararVectoresChar(vector1, vector2);
	}

	public static void compararVectoresChar(int vector1[], int vector2[]) { 

		int respuesta = 2;
		
		if (Arrays.equals(vector1, vector2)) {
			respuesta = 0;
		}else if (vector1.length > vector2.length) {
			respuesta = 1;
		}else {
			respuesta = -1;
		}

		System.out.println(respuesta);
	}
	
}
