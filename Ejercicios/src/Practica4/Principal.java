/**
 * Principal.java
 * Programa en entorno de pruebas al que al pasar dos vectores te informa si estos son iguales, en caso de que si mostrara un 0
 * en caso contrario mostrara un -1.
 * jmdb - 2021.01.07
 */

package Practica4;

public class Principal {
	
	final static class Usuario {

		public Usuario(String nif, String nombre, String apellidos, String correo, String domicilio, String fechaNacimiento,
				String fechaAlta, String claveAcceso, String rol) {
			setNif(nif);
			setNombre(nombre);
			setApellidos(apellidos);
			setCorreo(correo);
			setDomicilio(domicilio);
			setFechaNacimiento(fechaNacimiento);
			setFechaAlta(fechaAlta);
			setClaveAcceso(claveAcceso);
			setRol(rol);
		}
	    	    
		public String getNif() {
			return nif;
		}
		public void setNif(String nif) {
			this.nif = nif;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getApellidos() {
			return apellidos;
		}
		public void setApellidos(String apellidos) {
			this.apellidos = apellidos;
		}
		public String getCorreo() {
			return correo;
		}
		public void setCorreo(String correo) {
			this.correo = correo;
		}
		public String getDomicilio() {
			return domicilio;
		}
		public void setDomicilio(String domicilio) {
			this.domicilio = domicilio;
		}
		public String getFechaNacimiento() {
			return fechaNacimiento;
		}
		public void setFechaNacimiento(String fechaNacimiento) {
			this.fechaNacimiento = fechaNacimiento;
		}
		public String getFechaAlta() {
			return fechaAlta;
		}
		public void setFechaAlta(String fechaAlta) {
			this.fechaAlta = fechaAlta;
		}
		public String getClaveAcceso() {
			return claveAcceso;
		}
		public void setClaveAcceso(String claveAcceso) {
			this.claveAcceso = claveAcceso;
		}
		public String getRol() {
			return rol;
		}
		public void setRol(String rol) {
			this.rol = rol;
		}

		//Atributos
	    public String nif;
	    public String nombre;
	    public String apellidos;
	    public String correo;
	    public String domicilio;
	    public String fechaNacimiento;
	    public String fechaAlta;
	    public String claveAcceso;
	    public String rol;

	}

	final static int MAX_USUARIOS = 45;
	private static int posicionPatron = 0;
	private static int numUsuarioParaCargar = 1;

    // Almac�n de datos resuelto con arrays
    public static Usuario[] datosUsuarios = new Usuario[MAX_USUARIOS];
    public static void main(String[] args) {

        int[] patron = {0, 0, 1, 0, 0, 1, 1, 1};
        
        cargarDatosUsuarioPatron(patron);      
        
        for (int i = 0; i < datosUsuarios.length; i++) {
			System.out.println(datosUsuarios[i]);
		}
    }


	private static void cargarDatosUsuarioPatron(int[] patron) {
		
		for (int posicionUsuarios = 0; posicionUsuarios < datosUsuarios.length; posicionUsuarios++) { //El bucle para hacerlo solo 45 veces
			if (PatronCompletado(patron)) {
				reiniciarPatron();
			}
			if (PosicionParaGuardarDatos(patron)) {
				cargarDatosEnDatosUsuarios(posicionUsuarios);
				numUsuarioParaCargar++;
			}
			posicionPatron++;
		}
	}

		
	private static boolean PatronCompletado(int[] patron) {//Comprueba si el patron dado ha terminado o no
		return posicionPatron >= patron.length;
	}

	private static void reiniciarPatron() {//En caso de que el patron se acabe lo reinicia para empezar de nuevo
		posicionPatron = 0;
	}

	private static boolean PosicionParaGuardarDatos(int[] patron) {
		return patron[posicionPatron] == 1;
	}

	private static void cargarDatosEnDatosUsuarios(int posicionGuardar) {
		datosUsuarios[posicionGuardar] = new Usuario("nif" + numUsuarioParaCargar + numUsuarioParaCargar,
				" nombre" + numUsuarioParaCargar, " apellidos" + numUsuarioParaCargar, " correo" + numUsuarioParaCargar,
				" domicilio" + numUsuarioParaCargar, " fechaNacimiento" + numUsuarioParaCargar,
				" fechaAlta" + numUsuarioParaCargar, " claveAcceso" + numUsuarioParaCargar,
				"rol" + numUsuarioParaCargar);
	}
}
