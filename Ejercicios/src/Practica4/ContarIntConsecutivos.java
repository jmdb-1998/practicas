/**
 * ContarIntConsecutivos.java
 * Programa en entorno de pruebas al que al pasar un vectore te informa de cuantos numeros consecutivos hay en el vector.
 * jmdb - 2021.01.07
 */

package Practica4;

public class ContarIntConsecutivos {


	private static int [] vector1 = new int[] {3, 5, 3, 4, 5, 2, 3, 4};

	public static void main(String argumentos[]) {

		contarIntConsecutivos(vector1);
	}

	public static void contarIntConsecutivos(int vector1[]) { 

		int resultado = 0;

		for (int i = 0; i < vector1.length; i++) {
			
			if (i + 1 < vector1.length){
				
				int contador = vector1[i+1];
				
				if (vector1[i] == contador - 1) {
					
					resultado ++;	
				}else {
					
					resultado = 0;
				}
			}
		}

		System.out.println("Solo hay "+ (resultado + 1) + " numeros consecutivos en el vector");
	}

}
