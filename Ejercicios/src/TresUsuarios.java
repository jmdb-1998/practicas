import java.util.Scanner;

public class TresUsuarios {

	@SuppressWarnings({"resource" })
	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		
		String nombre1, nombre2, nombre3, apellidos1, apellidos2, apellidos3, nif1, nif2, nif3;
		int dpostal1, dpostal2, dpostal3;
		
		System.out.println("Introduzca los datos del primer alumno separados por un espacio");
		nombre1 = teclado.next();
		apellidos1 = teclado.next();
		nif1 = teclado.next();
		dpostal1 = teclado.nextInt();
		
		System.out.println("Introduzca los datos del segundo alumno separados por un espacio");
		nombre2 = teclado.next();
		apellidos2 = teclado.next();
		nif2 = teclado.next();
		dpostal2 = teclado.nextInt();
		
		System.out.println("Introduzca los datos del tercer alumno separados por un espacio");
		nombre3 = teclado.next();
		apellidos3 = teclado.next();
		nif3 = teclado.next();
		dpostal3 = teclado.nextInt();
		
		System.out.println(nombre3);
		System.out.println(apellidos3);
		System.out.println(nif3);
		System.out.println(dpostal3 + "\n");
		
		System.out.println(nombre2);
		System.out.println(apellidos2);
		System.out.println(nif2);
		System.out.println(dpostal2 + "\n");
		
		System.out.println(nombre1);
		System.out.println(apellidos1);
		System.out.println(nif1);
		System.out.println(dpostal1);
		
	}
}
