/**
 * Fibonacci.java
 * Programa al que le das una posicion en la sucesi�n de Fibonacci y te saca el valor de esta.
 * jmdb - 2020.10.09
 */

package Practica2;
import java.util.Scanner;

public class Fibonacci {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);

		int valor1 = 0;
		int valor2 = 1;
		int cuenta = 0;
		int valorfinal;


		System.out.println("Introduzca el numero de sucesiones que quiere calcular: ");
		valorfinal = teclado.nextInt();

		if (valorfinal > 0)
		{
			for (int i = 0; i < valorfinal; i++)
			{
				cuenta = valor1 + valor2;
				valor1 = valor2;
				valor2 = cuenta;	
			}

			System.out.println("El valor final en la secuancia de Fibonacci que has escogido es: " + cuenta);

		} else {
			System.out.println("Solo se admiten numeros positivos");	
		}


	}

}
