/**
 * DatosPersonaes.java
 * Programa simple que muestre en tres l�neas separadas tu nombre completo, calle, ciudad, pa�s y c�digo postal.
 * jmdb - 2020.10.27
 */

package Practica2;

public class DatosPersonales {

	public static void main(String[] args) {

		String nombre = "Juan Manuel Delgado Barranco";
		String calle = "Obispo Alburquerque";
		String ciudad = "Lorca";
		String pais = "Espa�a";
		int cod_Postal = 3800;

		System.out.println("Datos de Alumno: " + nombre);
		System.out.println("Direccion del Almuno: " + calle + ", " + ciudad + ", " + pais);
		System.out.println("Codigo postal: " + cod_Postal);
	}
}

