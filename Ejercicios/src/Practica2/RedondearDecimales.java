/**
 * RedondearDecimales.java
 * Programa al que si le das un n�mero decimal y la cantidad de decimales que quieres te lo muestra en pantalla
 * jmdb - 2020.11.01
 */
package Practica2;
import java.util.Scanner;

public class RedondearDecimales {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
	
		double numeros;
		int numero_decimales;
		double n = 1;
		double resultado;
		String response;
		
		do {
			System.out.println("Por favor introduzca un n�mero con decimales");
			numeros = teclado.nextDouble();
		
			System.out.println("�A cuantos decimales quiere redondearlos?");
			numero_decimales = teclado.nextInt();
		
			for (int i = 1; i <= numero_decimales; i++) {
				n = n * 10;
				
			}
			
			resultado = Math.round((numeros * n))/n;
			System.out.println("El redondeo del numero es " + resultado + "\n");
			
			System.out.println("�Desea continuar?");
			response = teclado.next();
		}

		while (response.equalsIgnoreCase("s"));
		if (response.equalsIgnoreCase("n")) {
			System.out.println("Fin del programa.");
		}
	}
	
}
