/**
 * SumaEnteros.java
 * Programa al que le introduces 8 n�meros enteros y te muestra la suma de todos ellos.
 * jmdb - 2020.11.01
 */

package Practica2;
import java.util.Scanner;

public class SumaEnteros {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
		
		int resultado = 0;
		int i = 1;
		
		while (i<= 8) {
			System.out.println("Por favor introduzca un n�mero");
			
			resultado += teclado.nextInt();
			
			i++;
		}
		
		System.out.println("La suma total de todos los valores es " + resultado);
	}
	
}
