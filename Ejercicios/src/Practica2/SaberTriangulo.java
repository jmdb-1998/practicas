/**
 * SaberTriangulos.java
 * Programa que requiere de 3 datos para saber que tipo de triangulo formarian los lados con esa longitud.
 * jmdb - 2020.10.09
 */

//NO ESTA ACABADO

package Practica2;
import java.util.Scanner;

public class SaberTriangulo {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);

		double lado1, lado2, lado3;
		double comprobante1, comprobante2, comprobante3;
		String response;

		do {

			System.out.println("Por favor introduzca u valor para el dato 1");
			lado1 = teclado.nextDouble();

			System.out.println("Por favor introduzca u valor para el dato 2");
			lado2 = teclado.nextDouble();

			System.out.println("Por favor introduzca u valor para el dato 3");
			lado3 = teclado.nextDouble();

			comprobante1 = lado1 + lado2;
			comprobante2 = lado2 + lado3;
			comprobante3 = lado1 + lado3;

			if(comprobante1 > lado3 || comprobante2 > lado1 || comprobante3 > lado2) {

				if (lado1 == lado2 && lado2 == lado3)
					System.out.println("\nEl Triangulo es Equilatero");
				else
				{
					if (lado1 == lado2 || lado1 == lado3 || lado2 == lado3)
						System.out.println("\nEl Triangulo es Isoceles");
					else
					{
						if (lado1!=lado2 || lado1!=lado3 || lado3!=lado2)
							System.out.println("\nEl Triangulo es Escaleno\n");
					}
				}

			} else {
				System.out.println("Los valores introducidos no son validos");
			}

			System.out.println("�Desea comprobar otro triangulo?");
			response = teclado.next();

			if (!response.equalsIgnoreCase("s") && !response.equalsIgnoreCase("n")) {
				System.out.println("Por favor, introduzca s o n");
				response = teclado.next();
			}

		}

		while (response.equalsIgnoreCase("s"));


		if (response.equalsIgnoreCase("n")){
			System.out.println("Fin del programa.");
		}
	}
}