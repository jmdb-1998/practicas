/**
 * CapitalTotal.java
 * Programa al que le introduces un capital inicial, un interes anual y el numero de a�os te saca el capital final.
 * jmdb - 2020.10.09
 */

package Practica2;
import java.util.Scanner;

public class CapitalTotal {
	static Scanner teclado = new Scanner(System.in);
	
	public static void main(String[] args) {

		int capital_inicial;
		double capital_final;
		int interes_anual;
		int a�os;
		double potencia;
		
		System.out.println("Por favor, introduzca capital inicial");
		capital_inicial = teclado.nextInt();
		System.out.println("�Cuanto interes quiere?");
		interes_anual = teclado.nextInt();
		System.out.println("�Cuantos a�os");
		a�os = teclado.nextInt();
		
		potencia = Math.pow(interes_anual + 1, a�os);
		
		capital_final = capital_inicial * potencia;
		
		System.out.println("Su capital final es " + capital_final);
	}
}