/**
 * MediaAritmetica.java
 * Programa simple que muestra un mensaje la consola del sistema.
 * jmdb - 2020.11.01
 */
//Faltaria realizar un fitro para numeros negativos distintos de 0

package Practica2;
import java.util.Scanner;

public class MediaAritmetica {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
	
		int numeros;
		int suma = 0;
		int media = 0;
		int resultado_media;
		
		do {
			System.out.println("Por favor introduzca un n�mero");
			numeros = teclado.nextInt();
			suma = suma + numeros;
			
			media++;
		
		}
		
		while (numeros > 0);
		if (numeros <= 0) {
			System.out.println("La suma de los " + media + " n�meros introducidos es " + suma + "\n");
			resultado_media = suma / media-1; 
			System.out.println("Y su media aritmetica es: " + resultado_media);
		}
	}
	
}
