/**
 * MultiplosTres.java
 * Programa simple que muestra por consola todos los m�ltiplos de 3 comprendidos entre 1 y 3000.
 * jmdb - 2020.11.04
 */
package Practica2;

public class MultiplosTres {

	public static void main(String[] args) {

        for (int i = 1; i <= 3000; i++) {
            if(i % 3 == 0){
                
            	 System.out.println("El numero " + i + " es multiplo de 3");
            }  

        }
       
	}

}

