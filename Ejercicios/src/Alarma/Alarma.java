package Alarma;

import java.util.Random;

public class Alarma {

	static Sensor sensor = new Sensor(); //Generamos un nuevo sensor para acceder a sus metodos

	public static Boolean comprobar(Double umbral) { //Este metodo comprueba si se debe de dar la alarma o no

		boolean test = sensor.getValorActual(umbral);
		
		return test;
	}

	public static double generarDoubleAleatorio() { //Generamos doubles aleatorios para comproba el funcionamiento de la Alarma
		Random rd = new Random();
		return rd.nextDouble();
	}

}
