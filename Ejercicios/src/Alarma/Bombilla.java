package Alarma;

public class Bombilla {

	private Boolean estado;

	public Boolean getEstado() {
		return estado;
	}
	
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}
	
	public void activar() {
		this.estado = true;
		luz(this.estado);
	}

	public void desActivar() {
		this.estado = false;
		luz(this.estado);
	}
	
	public void luz(Boolean estado) {
		if (this.estado == true) {
			System.out.println("Brilla");
		}else {
			System.out.println("No brilla");
		}
	}
}
