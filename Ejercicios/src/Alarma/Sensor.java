package Alarma;

public class Sensor {

	private Double valorActual;
	
	public Sensor(Double valor) {
		this.setValorActual(valor);
	}
	
	public Sensor() {
		this.setValorActual(0.0);
	}
	
	public Sensor(Sensor sensor) {
		this.valorActual = Double.valueOf(sensor.valorActual);
	}

	public boolean getValorActual(Double valorActual) { //Metodo que hace sonar la alarma si el umbral es superado
		Timbre t = new Timbre(); //Generamos un timbre
		if (valorActual >= 0.5) {
			t.activar();
			return true;
		}else {
			t.desActivar();
		}
		return false;
	}

	public void setValorActual(Double valorActual) {
		this.valorActual = valorActual;
	}
}
