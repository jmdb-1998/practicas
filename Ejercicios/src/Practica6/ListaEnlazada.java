/**
 * ListaEnlazada.java
 * Implementaci�n del Add y el RemoveAll en una lista Enlazada
 * jmdb - 2021.04.10
 */

package Practica6;

public class ListaEnlazada {
	// Atributos
	private Nodo primero;
	private int numElementos;

	public static void main(String[] args) {

		ListaEnlazada listaAdd = new ListaEnlazada();

		//se a�aden 5 datos de prueba con el m�todo tradicional al final de la lista de formas distintas.

		listaAdd.add("Patatas");
		listaAdd.add("Manzanas");
		listaAdd.add("Fresas");
		listaAdd.add("Sandia");
		listaAdd.add(4,"Cocos");

		System.out.println("Lista 1:\n");
		for (int i = 0; i < listaAdd.size(); i++) {
			System.out.println(i + ": " + listaAdd.get(i));
		}

		ListaEnlazada listaRemove = new ListaEnlazada();

		//A�adimos datos en la segunda lista para borrarlas en la primera
		listaRemove.add("Fresas");
		listaRemove.add("Manzanas");

		//Se borran los datos de la lista1 que est�n tambi�n en la lista2.
		listaAdd.removeAll(listaRemove);

		System.out.println("\nLista 1 modificada:\n");
		for (int i = 0; i < listaAdd.size(); i++) {
			System.out.println(i + ": " + listaAdd.get(i));

		}
	}

	public ListaEnlazada() {
		primero = null;
		numElementos = 0;
	}

	public int size() {

		return numElementos;
	}

	public void add(Object dato) {

		Nodo nuevo = new Nodo(dato);
		Nodo ultimo = null;

		if (numElementos == 0) {

			primero = nuevo;
		}

		else {

			ultimo = obtenerNodo(numElementos-1);
			ultimo.siguiente = nuevo;
		}

		numElementos++;
	}

	public void add(int indice, Object dato) {

		//Excepci�n que se lanza si intentamos introducir el n�mero en un �ndice fuera de los l�mites de la lista.
		if (indice < 0 || indice > numElementos) {
			throw new IndexOutOfBoundsException("El indice esta fuera del rango " + indice);
		}

		Nodo nuevo = new Nodo(dato);
		Nodo actual = obtenerNodo(indice);

		//caso en el que se quiera introducir el dato en primer lugar
		if(indice == 0) {

			primero = nuevo;
			nuevo.siguiente = actual;

		}

		else {

			//En el caso de que se quiera introducir el dato en �ltimo lugar. Se resta 1 a numElementos para evitar 
			//sumarlo dos veces (una con cada m�todo add)

			if(indice == numElementos) {

				this.add(dato);
				numElementos --;
			}

			else {

				Nodo anterior = obtenerNodo(indice-1);

				anterior.siguiente = nuevo;
				nuevo.siguiente = actual;
			}
		}

		numElementos ++;
	}

	private Nodo obtenerNodo(int indice) {
		assert indice >= 0 && indice < numElementos;

		Nodo actual = primero;
		for (int i=0;i<indice;i++) actual = actual.siguiente;
		return actual;
	}

	public void removeAll(ListaEnlazada datosAborrar) {

		for(int i=0;i<this.numElementos;i++) {

			for(int j=0;j<datosAborrar.numElementos;j++) {

				if(this.get(i) == datosAborrar.get(j)) {

					this.remove(i);
					numElementos --;
				}
			}
		}
	}

	//Elimina el elemento indicado por el �ndice. Ignora �ndices negativos

	public void remove(int indice) {

		if (indice >= numElementos || indice < 0) {
			throw new IndexOutOfBoundsException("El indice esta fuera del rango" + indice);
		}		
		if (indice > 0) {		
			removeIntermedio(indice);
		}		
		if (indice == 0) {
			removePrimero();
		}		
	}

	//M�todos auxiliares que ayudan al metodo remove a realizar su funci�n

	private void removePrimero() {

		primero = primero.siguiente;
		numElementos--;
	}

	private void removeIntermedio(int indice) {

		assert indice > 0 && indice < numElementos;

		Nodo actual = null;
		Nodo anterior = null;

		anterior = obtenerNodo(indice - 1);

		actual = anterior.siguiente;
		anterior.siguiente = actual.siguiente;
		numElementos--;
	}


	public Object get(int indice) {

		if (indice >= numElementos || indice < 0) {
			throw new IndexOutOfBoundsException("El indice esta fuera del rango" + indice);
		}
		Nodo aux = obtenerNodo(indice);
		return aux.dato;
	}

	class Nodo {
		// Atributos
		Object dato;
		Nodo siguiente;	


		public Nodo(Object dato) {
			this.dato = dato;
			siguiente = null;
		}

	}
}
