/**
 * PruebaPila.java
 * Demostraci�n de como funciona una Pila en java
 * jmdb - 2021.04.06
 */

package Practica6;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class PruebaPila {

	public static void main(String argumentos[]) {

		Stack<String> pila = new Stack<String>();
		pila.push("1. Camaro");
		pila.push("2. Leon");
		pila.push("3. Panda");
		pila.push("4. F40");

		//Convierte el Stack en array.
		Object[] coches = pila.toArray();

		//Muestra la pila de nombres de plantas.
		System.out.println("Cima de la pila: " + pila.peek());
		
		while (pila.size() > 0) {
			String nombreCoche = pila.pop();
			System.out.println(nombreCoche);
		}
		//Muestra el array de nombres de plantas. La pila ya est� vac�a.
		System.out.println("Array:\n" + Arrays.toString(coches));
	}

}
