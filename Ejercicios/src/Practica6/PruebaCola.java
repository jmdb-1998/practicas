/**
 * PruebaPila.java
 * Demostraci�n de como funciona una cola en java
 * jmdb - 2021.04.06
 */

package Practica6;
import java.util.LinkedList;
import java.util.Queue;

public class PruebaCola {

	public static void main(String argumentos[]) {
		
		Queue<String> cola1 = new LinkedList<String>();
		System.out.println("Insertamos tres elementos en la cola: Juan, Ana y Luis");
		
		cola1.add("Juan");
		cola1.add("Ana");
		cola1.add("Luis");
		
		System.out.println("Cantidad de elementos en la cola:" + cola1.size());
		System.out.println("Extraemos un elemento de la cola:" + cola1.poll());
		System.out.println("Cantidad de elementos en la cola:" + cola1.size());
		System.out.println("Consultamos el primer elemento de la cola sin extraerlo:" + cola1.peek());
		System.out.println("Cantidad de elementos en la cola:" + cola1.size());
		System.out.println("Extraemos uno a un cada elemento de la cola mientras no este vac�a:");
		
		while (!cola1.isEmpty())
			System.out.print(cola1.poll() + "-");
		System.out.println();
	}
}
