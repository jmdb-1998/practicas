/**
 * PruebaLiterales.java
 * Programa simple donde inicializamos variables para comprobar que son
 * ajp - 2020.10.09
 */

public class PruebaLiterales {
	@SuppressWarnings("unused")
	public static void main(String[] args) {	
		System.out.println("Hola mundo...");  //Muestra mensaje 
		
		double var01 = -11.1;
		
        double var02 = -88.28;
        
        double var03 = .3e27; // e tiene valo equivalente a 10^x
        
        double var04 = 23e2; //debido a e refiriendose a 10^x, no puede contener decimales
        
        String var05 = "cañón";
        
        double var06 = 0377; //El 0 hara que se interprete en hexadecimal: no decimal
        
        int var07 = 9999;
        
        // este nos dara error double var8 = 099;
        
        double var09 = +521.6;
        
        double var10 = 1.26;
        
        double var11 = 5E-002;
        
        double var12 = 0xFE; //representado en hexadecimal: su valor decimal es 254
        
        double var13 = 0b101010; //representado en binario: su valor decimal es 42
        
        double var14 = 1.26f; //f se refiere a que es de tipo float	
        
        char var15 = '\n'; //usado para guardar un caracter: en este caso para un enter:
        
        //String var16 = while; while no es un valor valido ya que es una palabra reservada.
        
        //double var17 = \xFE; \xFE no puede guardarse como valor tiene caracteres no validos
        
        double var18 = 1234;
        
        double var19 = .567;
        
        double var20 = 0xFFFE; //Es un valor hexadecimal: su valor real es 65534
        
        //double var21 = XGA; XGA no puede guardarse como valor 
        
        String var21 = "a";
        
        // String var22 = 'abc'; los caracteres '' se usan para caracteres sueltos, no grupos de caracteres
        
        double var22 = 02.45;
        
        char var23 = 'a';
        
        //double var24 = xF2E;  no puede guardarse como valor: si es un numero hexadecimal debe añadirse un 0 delante 
        
        double var25 = 0xf;
        
        //String var26 = abc; no puede guardarse como valor 
        
        //String var27 = ab"c; no puede guardarse como valor 
        
        //String var28 = "abc;  no puede guardarse como valor 
        
        //String var29 = "abc';  no se pueden utilizar distintos tipos de comillas 
        
        //String var30 = a';  no puede guardarse como valor 
        
        String var31 = "abc;";
        
        String var32 = "abc'";
        
//        String var33 = "abc""; Este valor no es valido ya que las comillas no se cierran
        
//      String var34 = ""abc""; Este valor no es valido ya que las "" se cierran a si mismas.
        
        String var35 = "true";
        
        //boolean var36 = True; para que este funcione, debe escribirse en minusculas
        
        boolean var37 = false;
        
        char var38 = '\\';
              
    }
 
}