/**
 * Multiplo4.java
 * Programa que pipde un numero y muestra los multiplos de 2 hasta llegar a ese tope.
 * jmdb - 2020.11.28
 */

package Practica3;
import java.util.Scanner;

public class Multiplo4 {

	public static void main(String argumentos[]) {

		Scanner teclado = new Scanner(System.in);
		mostrarMultiplos(pedirTope());
		
		teclado.close();
	}
	
	public static int pedirTope() {
		Scanner teclado = new Scanner(System.in);
		int tope;
		
		System.out.println("Por favor introduzca el tope: ");
		return tope = teclado.nextInt();
		
	}
	
	public static void mostrarMultiplos(int tope) {
		int mult = 0;	
		int cont = 0;

		System.out.println("\t M�ltiplos de 2\n");
		while (mult < tope)
		{
			mult = cont * 2;
			System.out.println("\t  " +    '#' + (cont+1) + '\t' + mult);
			++cont;
		}
	}

}

