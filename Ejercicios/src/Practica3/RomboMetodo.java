/**
 * RomboMetodo.java
 * Programa que funciona similarmente al otro programa llamado Rombo, salvo que muestra el rombo como una cadena de texto formateada
 * jmdb - 2020.12.02
 */

package Practica3;

import java.util.Scanner;

public class RomboMetodo {
    
    public static void main(String[] args) {
    	
    	Scanner teclado = new Scanner(System.in);
    	
        boolean numeroCorrecto = false;
        int numFilasRombos; 
        
        do {
            System.out.print("Introduce el n�mero de filas (natural e impar): ");
            
            numFilasRombos = teclado.nextInt();
            
            if(numFilasRombos > 0 && numFilasRombos % 2 != 0){
                numeroCorrecto = true;
            }
        } while (!numeroCorrecto);
         
        rombo2(numFilasRombos);
    }
	
    public static void rombo2(int numFilasRombos) {

    	String resultado ="";
    	
		System.out.println("");
        
        int numFilas = numFilasRombos/2 + 1;
                 
        for(int altura = 1; altura <= numFilas; altura++){
            
            for(int blancos = 1; blancos <= numFilas - altura; blancos++){
                resultado += " ";
            }
            
            for(int asteriscos = 1; asteriscos <= (2 * altura) - 1; asteriscos++){
                resultado += "*";
            }
            resultado += "\n";
        }
         
        numFilas--;
         
        for(int altura = 1; altura <= numFilas; altura++){
            
            for(int blancos = 1; blancos <= altura; blancos++){
                resultado += " ";
            }
            
            for(int asteriscos = 1; asteriscos <= (numFilas - altura) * 2 + 1; asteriscos++){
            	resultado += "*";
            }
            resultado += "\n";
        }
        System.out.print(resultado);
	}	
}

