/**
 * TrianguloMetodoTexto.java
 * Programa que funciona similarmente al otro programa llamado triangulo, salvo que muestra el triangulo como una cadena de texto formateada
 * jmdb - 2020.12.01
 */

package Practica3;

import java.util.Scanner;

public class TrianguloMetodoTexto {

	public static void main(String argumentos[]) {

		Scanner teclado = new Scanner(System.in);

		System.out.println("Por favor introduzca el numero de niveles de la piramide ");
		int numero = teclado.nextInt();
		
		trianguloTexto(numero);
		
		teclado.close();
	}

	public static String asteriscos(int numero) {

		String asteriscos = "";

		for (int i = -1; i < (numero * 2); i++) {
			asteriscos += "*";
		}	
		return asteriscos;
	}

	public static String espacios(int numero) {

		String espacios = "";

		for (int i = numero; i > 1; i--) {
			espacios += " ";
		}
		return espacios;
	}
	
	public static String trianguloTexto(int numero) {

		int contador = numero;
		String salto = "\n";
		String triangulo = "";
		
		for (int i = 0; i < numero; i++) {

			triangulo = (espacios(contador) + asteriscos(i)) + salto;
			contador--;
			System.out.print(triangulo);
		}
		return triangulo;
	}
}
