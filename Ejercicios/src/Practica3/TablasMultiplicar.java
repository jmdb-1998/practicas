/**
 * TablasMultiplicar.java
 * Programa que Introduces un n�mero y te realiza la tabla de este hasta el 15.
 * jmdb - 2020.11.24
 */

package Practica3;

import java.util.Scanner;

public class TablasMultiplicar {

	Scanner teclado = new Scanner(System.in);

	public static void main(String argumentos[]) {

		Scanner teclado = new Scanner(System.in);
		String response;

		System.out.println("Por favor, introduzcal el numero para saber su tabla de multiplicar");
		int numero = teclado.nextInt();

		calcularMostrarTabla(numero);

		teclado.close();
	}

	public static void calcularMostrarTabla(int numero) {
		int i = 1;	
		int tope = 15;
		int resultado;

		while (i <= tope)
		{
			resultado = numero * i;
			System.out.println("\t  " +    '#' + (i) + '\t' + resultado);
			i++;
		}
	}	

}
