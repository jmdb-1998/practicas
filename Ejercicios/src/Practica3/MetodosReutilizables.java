package Practica3;

public class MetodosReutilizables {

	public static void comprobarNumero(int valorfinal) {

		if (valorfinal < 0)
		{
			System.out.println("Solo se admiten numeros positivos");
		}
	}
	
	public static void comprobarNumeroD(double valorfinal) {

		if (valorfinal < 0)
		{
			System.out.println("Solo se admiten numeros positivos");
		}
	}
}
