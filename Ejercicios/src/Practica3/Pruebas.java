// Multiplo1.java
		// Fecha: 14/10/2013
		// Autor/a:.......
		// Escribe en pantalla los m�ltiplos de 2 por debajo de un TOPE, 16.

		
package Practica3;
import java.util.Scanner;

public class Pruebas {


			public static void main(String argumentos[]) {

				// Declaraci�n de variables
				Scanner teclado = new Scanner(System.in);
				final int TOPE = 16  ; 				// Constante, el m�ximo valor del m�ltiplo
				int mult  ;							// Almacena el m�ltiplo calculado
				int cont  ;							// Contador utilizado en el c�lculo

				// Inicializa las variables
				mult = 0 ;
				cont = 0 ;

				System.out.println("\t M�ltiplos de 2\n");
				while (mult < TOPE)					// Bucle de c�lculo y visualizaci�n
				{
					mult = cont * 2;
					System.out.println("\t  " +    '#' + (cont+1) + '\t' + mult);
					++cont;
				}
				teclado.close();
			}
		}

