/**
 * FibonacciMetodo.java
 * Programa que Introduces un n�mero y te realiza la sucesi�n de fibonacci hasta ese numero de saltos.
 * jmdb - 2020.11.26
 */

package Practica3;

import java.util.Scanner;

public class FibonacciMetodo {

	public static void main(String argumentos[]) {

		Scanner teclado = new Scanner(System.in);


		System.out.println("Introduzca el numero de sucesiones que quiere calcular: ");
		int valorfinal = teclado.nextInt();

		MetodosReutilizables.comprobarNumero(valorfinal);
		fibonacci(valorfinal);

		teclado.close();
	}

	public static void fibonacci(int valorfinal) {

		int valor1 = 0;
		int valor2 = 1;
		int cuenta = 0;

		for (int i = 0; i < valorfinal; i++) //En este for se realizan la sumas para mostrar el resultdo final
		{
			cuenta = valor1 + valor2;
			valor1 = valor2;
			valor2 = cuenta;	
		}

		System.out.println("El valor final en la secuancia de Fibonacci que has escogido es: " + cuenta);
	} 	
}
