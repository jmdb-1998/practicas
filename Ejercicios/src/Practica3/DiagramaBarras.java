/**
 * DiagramaBarras.java
 * Programa en entorno de pruebas al que al pasar una serie de valores fijos los muestra a la izquierda o derecha
 * de una "|" dependiendo de su signo.
 * jmdb - 2020.12.02
 */

package Practica3;

import java.util.Scanner;

public class DiagramaBarras {

	//Variables necesarias en el metodo
	private static final int LIMITE_SUPERIOR = 11;
	private static final int LIMITE_INFERIOR = -11;
	private static final int topeBlancos = 9;

	public static void main(String argumentos[]) {

		Scanner teclado = new Scanner(System.in);
		int dato1, dato2, dato3, dato4, dato5, dato6;
		dato1 = 2; dato2 = 7; dato3 = -13; dato4 = -6; dato5 = -8; dato6 = 18;
		
		generarBarraHorizontal(dato1);
		generarBarraHorizontal(dato2);
		generarBarraHorizontal(dato4);
		generarBarraHorizontal(dato5);
		generarBarraHorizontal(dato6);
		generarBarraHorizontal(dato3);

		teclado.close();
	}

	public static void generarBarraHorizontal(int numero) {

		if (numero <= LIMITE_INFERIOR || numero >= LIMITE_SUPERIOR) { //Comprobante de que el numero esta dentro de los valores impuestos

			System.out.print(numero + " FUERA DE RANGO" + "\n");
			
		} else if (numero < 0){ //Aqui se crean las lineas que seran imprimidas en pantalla
			
			numero = numero * -1;		
			System.out.print(numero * -1 + "\t" + espacios(topeBlancos - numero) + generarBloqueCaracteres(numero) + "|" + "\n");
			
		}else {
			
			System.out.print(numero + "\t" + espacios(topeBlancos) + "|" + generarBloqueCaracteres(numero) + "\n");
		}
	}

	public static String generarBloqueCaracteres(int numero) { //Se generan los bloques de *

		String resultado = "";

		for (int i = 0; i < numero; i++) {
			resultado += "*";
		}	
		return resultado;
	}
	
	public static String espacios(int numero) {//Aqui se generan los bloques de blancos necesarios para que se vea como ponen en el ejercicio

		String resultado = "";

		for (int i = numero; i > 1; i--) {
			resultado += " ";
		}
		return resultado;
	}

}
