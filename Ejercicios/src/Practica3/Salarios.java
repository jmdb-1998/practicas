/**
 * Salarios.java
 * Programa que muestra por consola el salario que le le debe dar a un empleado por sus horas trabajadas, teniendo en cuenta las horas extra.
 * jmdb - 2020.12.01
 */

package Practica3;

import java.util.Scanner;

public class Salarios {

	Scanner teclado = new Scanner(System.in);

	public static void main(String argumentos[]) {

		Scanner teclado = new Scanner(System.in);
		String response;
		
		do {
			System.out.println("Por favor, introduzcal el numero de horas");
			int horas = teclado.nextInt();

			calcularSalario(horas);

			System.out.println("�Desea introducir otra cantidad de horas?");
			response = teclado.next();
		}

		while (response.equalsIgnoreCase("s"));
		if (response.equalsIgnoreCase("n")) {
			System.out.println("Fin del programa.");
		}

		teclado.close();
	}

	public static void calcularSalario(int horas) { //Aqui realizamos las cuentas para saber el salario final
		int i = 1;	
		int sueldo = 0;

		while (i <= horas)
		{
			sueldo = sueldo + 15;
			i++;

			if (i > 35) { //Si las horas son mayor que 35 la hora se paga a 22�

				sueldo = sueldo + 22;
				i++;
			}
		}

		System.out.println("La cantidad de dinero que debe pagar al empleado es " + sueldo + "\n");
	}	
}



