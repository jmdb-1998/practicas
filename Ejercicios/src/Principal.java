public class Principal {
	
	final static class Usuario {

	    //Atributos
	    public String nif;
	    public String nombre;
	    public String apellidos;
	    public String correo;
	    public String domicilio;
	    public String fechaNacimiento;
	    public String fechaAlta;
	    public String claveAcceso;
	    public String rol;

	} //class

	final static int MAX_USUARIOS = 45;

    // Almac�n de datos resuelto con arrays
    public static Usuario[] datosUsuarios = new Usuario[MAX_USUARIOS];
    public static void main(String[] args) {

        int[] patron = {1, 0, 1, 0, 0, 1};
        
        cargarDatosUsuarioPatron(patron);
        
        mostrarDatos();
    }


	private static void cargarDatosUsuarioPatron(int[] patron) {
		
		for (int i = 0; i < MAX_USUARIOS; i++) {
			datosUsuarios[i] = new Usuario();
		}
		
	}
	
	private static void mostrarDatos() {
		
		for (Usuario usr : datosUsuarios) {
			System.out.println(usr);
		}
		
	}

    //...
	
}
