/**
 * ejAutoevaluacion.java
 * Escritura de un programa que lee una serie de datos por teclado y despu�s muestra por pantalla el mayor y el menos de todos ellos.
 * JAFM 7/11/20
 */

import java.util.Scanner;

public class ejAutoevaluacion {

	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
		
		int valorEntrada, contadorDatosValidos=1;
		int ElMayor=0, ElMenor=0;
		String terminar;
		
		System.out.println("Teclea n�meros entre el 1 y el 123456 y pulsa 0 para terminar");
		valorEntrada = teclado.nextInt();
		ElMenor=valorEntrada;
		ElMayor=valorEntrada;
		
		do {
			valorEntrada = teclado.nextInt();
			
			if(valorEntrada >= 1 && valorEntrada <= 123456) {
				contadorDatosValidos++;
				if(valorEntrada < ElMenor) ElMenor=valorEntrada;
				if(valorEntrada > ElMayor) ElMayor=valorEntrada;
			}
			
			if(valorEntrada < 0 && valorEntrada > 123456) System.out.println("No es un n�mero v�lido!");
			
		}
		
		while(valorEntrada !=0);
		
		System.out.println("Seguro que quiere finalizar? Pulsa t");
		terminar = teclado.next();
		
		if (terminar.equals("t")) {

		System.out.println("\nResultados:");
		System.out.println("\nTotal de n�meros contados: " +contadorDatosValidos);
		System.out.println("El mayor es: " + ElMayor);
		System.out.println("El menor es: " + ElMenor);

		System.out.println("\nFin del ejercicio");
			
		}
		teclado.close();
	}
}	

