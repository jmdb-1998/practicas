package Practica7Ejercicio2;

public class OrdenadorTienda {
	
	private String marca;
	private String modelo;
	private String precio;
	private String descripcion;
	
	
	public OrdenadorTienda() {
		
		this.setMarca("Lenovo");
		this.setModelo("Thinkpad 307");
		this.setPrecio("399,99");
		this.setDescripcion("portatil perfecto para el trabajo");
	}
	
	public OrdenadorTienda(String marca, String modelo, String precio, String descripcion) {
		
		this.setMarca(marca);
		this.setModelo(modelo);
		this.setPrecio(precio);
		this.setDescripcion(descripcion);
	}
	
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getPrecio() {
		return precio;
	}
	public void setPrecio(String precio) {
		this.precio = precio;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
