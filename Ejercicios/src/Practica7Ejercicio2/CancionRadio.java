package Practica7Ejercicio2;

public class CancionRadio extends Cancion {


	public CancionRadio() {

		this.setArtista(null);
		this.setGenero(null);
		this.setTitulo(null);
		this.setDuracion("0");
	}

	public CancionRadio(String titulo, String artista, String duracion, String genero) {

		this.setArtista(artista);
		this.setGenero(genero);
		this.setTitulo(titulo);
		this.setDuracion(duracion);

	}
}
