package Practica7Ejercicio2;

public class Manzana {

	private String precioporKilo;
	private String tipo;

	public Manzana() {

		this.setTipo("Golden");
		this.setPrecioporKilo("0,56");
	}

	public Manzana(String tipo, String precio) {

		this.setTipo(tipo);
		this.setPrecioporKilo(precio);
	}
	
	public Manzana(Manzana manzana) {
		
		this.tipo = new String(manzana.tipo);
		this.precioporKilo = new String(manzana.precioporKilo);
	}

	public String getPrecioporKilo() {
		return precioporKilo;
	}
	public void setPrecioporKilo(String precioporKilo) {
		this.precioporKilo = precioporKilo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


}
