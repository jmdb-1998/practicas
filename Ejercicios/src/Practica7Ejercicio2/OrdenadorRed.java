package Practica7Ejercicio2;

public class OrdenadorRed {

	private String due�o;
	private String ip;
	private String mac;

	public OrdenadorRed() {

		this.setDue�o("Empleado no1");
		this.setIp("255.138.116.222");
		this.setMac("ea:bc:73:5f:6f:c2");
	}

	public OrdenadorRed(String due�o, String ip, String mac) {

		this.setDue�o(due�o);
		this.setIp(ip);
		this.setMac(mac);
	}

	public String getDue�o() {
		return due�o;
	}
	public void setDue�o(String due�o) {
		this.due�o = due�o;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
}
