package Practica7Ejercicio2;

public class SegmentoRecta {

	private int ptoInicio;
	private int ptoFinal;
	
	
	public SegmentoRecta() {
		
		this.setPtoInicio(0);
		this.setPtoFinal(1);
	}
	
	public SegmentoRecta(int punto1, int punto2) {
		
		this.setPtoInicio(punto1);
		this.setPtoFinal(punto2);
	}
	
	public int getPtoInicio() {
		return ptoInicio;
	}
	public void setPtoInicio(int ptoInicio) {
		this.ptoInicio = ptoInicio;
	}
	public int getPtoFinal() {
		return ptoFinal;
	}
	public void setPtoFinal(int ptoFinal) {
		this.ptoFinal = ptoFinal;
	}
	
}
