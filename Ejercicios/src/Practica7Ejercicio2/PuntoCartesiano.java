package Practica7Ejercicio2;

public class PuntoCartesiano {

	private int ejeX;
	private int ejeY;

	public PuntoCartesiano() {

		this.setEjeX(0);
		this.setEjeY(0);
	}

	public PuntoCartesiano(int x, int y) {

		this.setEjeX(x);
		this.setEjeY(y);
	}

	public int getEjeX() {
		return ejeX;
	}
	public void setEjeX(int ejeX) {
		this.ejeX = ejeX;
	}
	public int getEjeY() {
		return ejeY;
	}
	public void setEjeY(int ejeY) {
		this.ejeY = ejeY;
	}

}
