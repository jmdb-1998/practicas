package Practica7Ejercicio2;

public class TelefonoTienda {

	private String marca;
	private String modelo;
	private String sistemaOperativo;
	private String precio;
	private boolean operadora;
	private String nombreOperadora;

	public TelefonoTienda(){

		this.setMarca("Samsung");
		this.setModelo("Galaxy S7");
		this.setSistemaOperativo("Android");
		this.setPrecio("299,99");
		this.setOperadora(true);
		this.setNombreOperadora("Vodafone");
	}

	public TelefonoTienda(String marca, String modelo, String sistemaOperativo,String precio, boolean operadora, String nombreOperadora){

		this.setMarca(marca);
		this.setModelo(modelo);
		this.setSistemaOperativo(sistemaOperativo);
		this.setPrecio(precio);
		this.setOperadora(operadora);
		this.setNombreOperadora(nombreOperadora);
	}

	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getPrecio() {
		return precio;
	}
	public void setPrecio(String precio) {
		this.precio = precio;
	}
	public boolean isOperadora() {
		return operadora;
	}
	public void setOperadora(boolean operadora) {
		this.operadora = operadora;
	}
	public String getNombreOperadora() {
		return nombreOperadora;
	}
	public void setNombreOperadora(String nombreOperadora) {
		this.nombreOperadora = nombreOperadora;
	}
	
	public String getSistemaOperativo() {
		return sistemaOperativo;
	}

	public void setSistemaOperativo(String sistemaOperativo) {
		this.sistemaOperativo = sistemaOperativo;
	}
}
