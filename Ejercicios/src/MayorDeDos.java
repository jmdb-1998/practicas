/**
 * Saludos.java
 * Programa simple que muestra varios mensajes en la consola del sistema.
 * ajp - 2020.10.09
 */

import java.util.Scanner;

public class MayorDeDos {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);

		int num1;
		int num2; 

		System.out.println("Por favor, introduzca el primer n�mero");
		num1 = teclado.nextInt();
		System.out.println("Por favor, introduzca el segundo n�mero");
		num2 = teclado.nextInt();
		if (num1 > num2) 
		{ 
			System.out.println(num1 + " es mayor que " + num2);
		}else {
			System.out.println(num2 + " es mayor que " + num1);
		}
	}
}
