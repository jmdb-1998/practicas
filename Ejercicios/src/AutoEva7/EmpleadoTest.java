/**
 * EmpleadoTest.java
 * Tests para la clase empleado
 * jmdb - 2021.05.22
 */

package AutoEva7;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import AutoEva7.Empleado.EmpleadoException;

class EmpleadoTest {

	private static Empleado empleado;
	private static Empleado empleado2;

	@BeforeAll
	public static void inicializarEmpleado() throws EmpleadoException {
		empleado = new Empleado("Paco", 13500, 58, Empleado.PuestoTrabajo.Jefe);
		empleado2 = new Empleado("Antonio", 11400, 23, Empleado.PuestoTrabajo.Empleado);
	}

	@Test
	public void testUsuarioCopia() {
		Empleado empleado = empleado2.clone();
		assertNotSame(empleado, empleado2);
		assertNotSame(empleado.getNombre(), empleado2.getNombre());
		assertNotSame(empleado.getSueldo(), empleado2.getSueldo());
		assertNotSame(empleado.getEdad(), empleado2.getEdad());
		assertNotSame(empleado.getPuesto(), empleado2.getPuesto());
	}

	@Test
	public void testSetNombre() throws EmpleadoException {
		empleado.setNombre("Nacho");
		assertEquals(empleado.getNombre(), "Nacho");
	}

	@Test
	public void testNombreNull() throws EmpleadoException {
		try {
			empleado.setNombre(null);
			fail("No debe llegar aqu�...");
		} catch (AssertionError exception) {
		}
	}

	@Test
	public void testSetSueldo() throws EmpleadoException {
		empleado.setSueldo(10500);
		assertEquals(empleado.getSueldo(), 10500, 00);
	}

	@Test
	public void testSetSueldoBajo() throws EmpleadoException {
		empleado.setSueldo(500);
		fail("No debe llegar aqu�...");
	}

	@Test
	public void testSetEdad() throws EmpleadoException {
		empleado.setEdad(20);
		assertEquals(empleado.getEdad(), 20);
	}

	@Test
	public void testSetEdadInvalida() throws EmpleadoException {
		empleado.setEdad(15);
		fail("No debe llegar aqu�...");
	}

	@Test
	public void testSetPuesto() {
		empleado.setPuesto(Empleado.PuestoTrabajo.Jefe);
		assertEquals(empleado.getPuesto(), Empleado.PuestoTrabajo.Jefe);
	}

	@Test
	public void testSetPuestoNull() {
		try {
			empleado.setPuesto(null);
			fail("No debe llegar aqu�...");
		} catch (AssertionError exception) {
		}
	}

	@Test
	public void testToString() {
		assertEquals(empleado.toString(), "Empleado [nombre=" + empleado.getNombre() + ", sueldo="
				+ empleado.getSueldo() + ", edad=" + empleado.getEdad() + ", puesto=" + empleado.getPuesto() + "]");
	}

}