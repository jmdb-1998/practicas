/**
 * Empleado.java
 * Clase empleado con los metodos para comprobar el sueldo y la edad
 * jmdb - 2021.05.22
 */

package AutoEva7;

public class Empleado {

	private String nombre;
	private int sueldo;
	private int edad;
	private PuestoTrabajo puesto;

	public enum PuestoTrabajo {Jefe, Empleado}

	public Empleado() {

		this.setNombre("Vacio");
		this.setSueldo(10000);
		this.setEdad(27);
		this.setPuesto(null);
	}

	public Empleado(String nombre, int sueldo, int edad, PuestoTrabajo puesto) {

		this.setNombre(nombre);
		this.setSueldo(sueldo);
		this.setEdad(edad);
		this.setPuesto(puesto);
	}

	public Empleado(Empleado empleado) {

		this.nombre = new String(empleado.nombre);
	}

	private boolean MayorEdad(int edad) {
		if (edad < 16 || edad > 65) {
			return false;
		}
		return true;
	}

	private boolean SueldoJusto(int sueldo) {
		if (sueldo < 10000) {
			return false;
		}	
		return true;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getSueldo() {
		return sueldo;
	}
	public void setSueldo(int sueldo) {
		if(SueldoJusto(sueldo) == true) {
			this.sueldo = sueldo;
		}
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		if(MayorEdad(edad) == true) {
			this.edad = edad;
		}
	}
	public PuestoTrabajo getPuesto() {
		return puesto;
	}
	public void setPuesto(PuestoTrabajo puesto) {
		this.puesto = puesto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + edad;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((puesto == null) ? 0 : puesto.hashCode());
		result = prime * result + sueldo;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empleado other = (Empleado) obj;
		if (edad != other.edad)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (puesto == null) {
			if (other.puesto != null)
				return false;
		} else if (!puesto.equals(other.puesto))
			return false;
		if (sueldo != other.sueldo)
			return false;
		return true;
	}
	
	public Empleado clone() {
		return new Empleado(this);
	}	

	@Override
	public String toString() {
		return "Empleado [nombre=" + nombre + ", sueldo=" + sueldo + ", edad=" + edad + ", puesto=" + puesto + "]";
	}

	/**
	 * Clase creada para tratar los posibles errores de la clase Empleado
	 *
	 */
	class EmpleadoException extends Exception {

		private static final long serialVersionUID = 1L;

		public EmpleadoException(String mensaje) {
			super(mensaje);
		}

		public EmpleadoException() {
			super();
		}
	}
}
