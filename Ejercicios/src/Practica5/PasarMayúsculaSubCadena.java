/**
 * PasarMay�sculaSubCadena.java
 * Programa que pasa a mayusculas las palabras que estan entre las dos etiquetas en el texto dado
 * jmdb - 2021.02.19
 */

package Practica5;

public class PasarMay�sculaSubCadena {

	public static void main(String argumentos[]) {

		String cadena = "<mayus>Estamos</mayus> viviendo en un <mayus>submarino amarillo</mayus>. No tenemos <mayus>nada</mayus> qu� hacer";

		String buscar1 = "<mayus>";
		String buscar2 = "</mayus>";

		System.out.print(pasarMay�sculaSubCadena(cadena, buscar1, buscar2));

	}

	public static String pasarMay�sculaSubCadena(String cadena, String buscar1, String buscar2) { 	//TO-DO

		while(cadena.indexOf(buscar1) != -1 || cadena.indexOf(buscar2) != -1) {

			
			int index1 = cadena.indexOf(buscar1);
			int index2 = cadena.indexOf(buscar2);
			int longitud = buscar2.length();
			
			String subcadena = cadena.substring(index1, index2 + longitud);
			
			cadena = cadena.replaceAll(subcadena, subcadena.toUpperCase());
			cadena = cadena.replaceAll(buscar1.toUpperCase(), "");
			cadena = cadena.replaceAll(buscar2.toUpperCase(), "");
			
		}
		
		return cadena;

	}

}
