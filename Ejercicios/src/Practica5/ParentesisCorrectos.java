/**
 * ParentesisCorrectos.java
 * Programa que infoma al usuario si falta algun partentesi en la formula dada
 * jmdb - 2021.02.17
 */

package Practica5;

public class ParentesisCorrectos {
	
	private static boolean resultado;

	public static void main(String argumentos[]) {

		String cadena = "((a + b) / 5-d))";
		
		parentesisCorrectos(cadena);
		
		if (resultado == true) {
			System.out.println("Los parentesis est�n bien emparejados");
		}else {
			System.out.println("Los parentesis no est�n bien emparejados");
		}
		
	}

	public static Boolean parentesisCorrectos(String cadena) { 
		
		int contador = 0;	
		
		for (int i = 0; i < cadena.length(); i ++) {
			
			if (cadena.substring(i,i+1).equals("(")) {
				contador ++;
				
			}if (cadena.substring(i,i+1).equals(")")) {
				contador --;
			}
			
		}
		
		if (contador == 0) {
			
			resultado = true;
		}
		
		return resultado;
	}
	
}
