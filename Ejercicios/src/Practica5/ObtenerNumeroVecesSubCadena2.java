/**
 * ObtenerNumeroVecesSubCadena2.java
 * Programa que obtiene el numero de veces que aparace una palabra en un texto dado sin discriminar mayusculas y minusculas
 * jmdb - 2021.02.17
 */

package Practica5;

public class ObtenerNumeroVecesSubCadena2 {

	public static void main(String argumentos[]) {

		String cadena = "Estamos viviendo en un submarino amarillo. No tenemos nada que hacer."  
				+"En el interior del submarino se est� muy apretado. As� que estamos leyendo todo el d�a." 
				+"Vamos a salir en 5 d�as";

		String buscar = "en";

		obtenerNumeroVecesSubCadena2(cadena.toLowerCase(), buscar);



	}

	public static void obtenerNumeroVecesSubCadena2(String cadena, String buscar) { 	

		int posicionCadena = 0;
		int control = 0;
		int contador = 0;

		for (int i = 0; i < cadena.length(); i++) { //Comenzamos el bucle para recorrer todo el String

			posicionCadena = cadena.indexOf(buscar, i); //Le damos el valor del indexOf a la variable posicionCadena
							
				if (posicionCadena > control) { //Si la variable es mayor que el control el contador aumenta y el control pasa a tener un valor mayor
					control = posicionCadena;
					contador ++;
				}

			}

			System.out.println(contador);
		}
	}
