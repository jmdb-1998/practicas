/**
 * PadRight.java
 * Programa que pasa a la izquierda el texto y el resto de espacio lo compreta con *
 * jmdb - 2021.02.17
 */

package Practica5;

public class PadRight {

	public static void main(String argumentos[]) {

		String cadena = "Introducción";
		String relleno = "*";
		int maximo = 20;

		padRight(cadena, relleno, maximo);

	}

	public static void padRight(String cadena, String relleno, int maximo) { 
		
		StringBuilder resultado = new StringBuilder(maximo);
		
		resultado.append(cadena);
		
		
		for (int i = 0; i < maximo-cadena.length(); i++) {
			
			resultado.append(relleno);
		}
		
		System.out.print(resultado);
	}
}