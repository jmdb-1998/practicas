/**
 * PruebaLiterales2.java
 * Programa simple donde inicializamos las variables mediante wrappers y mostramos el valor por pantalla
 * ajp - 2020.11.09
 */

public class PruebaLiterales2 {
	@SuppressWarnings({ "deprecation"})
	public static void main(String[] args) {	
		System.out.println("Hola mundo...");  //Muestra mensaje
	
	Double a = new Double(-11.1);
	
    Double b = new Double (-88.2);
    
    Double c = new Double (.3e27);
    
    String d = new String ("ca��n");
    
    Integer e = new Integer (0377);//La salida se muestra en decimal a pesar de que esta en octal
    
    Integer f = new Integer (9999);
    
    Double g = new Double (+521.6);//En la salida se omite el signo
 
    Double h = new Double (1.26);
    
    Double i = new Double (5E-002);//La salida esta en decimal sin base 10
    
    Integer j = new Integer (0xFE);//La salida se muestra en decimal 
    
    Integer k = new Integer (0b101010);//La salida se muestra en decimal 
    
    Double l = new Double (1.26f);//La salida se muestra en decimal 
    
    Integer m = new Integer (1234);
    
    Double n = new Double (.567);
    
    Integer o = new Integer (0xFFFE);//La salida se muestra en decimal 
    
    String p = new String ("a");
    
    Double q = new Double (02.45);//Omite el 0
    
    Character r = new Character  ('a');
    
    Integer s = new Integer (0xf);
    
    String t = new String ("true");
    
    Boolean u = new Boolean (false);
    
    String v = new String ("abc;");
    
    String w = new String ("abc'");
    
    Double x = new Double (23e2);//La salida se muestra en decimal
    
    
    //Mostraremos los valores de las variables por pantalla
    
    System.out.println(a);
    System.out.println(b);
    System.out.println(c);
    System.out.println(d);
    System.out.println(e);
    System.out.println(f);
    System.out.println(g);
    System.out.println(h);
    System.out.println(i);
    System.out.println(j);
    System.out.println(k);
    System.out.println(l);
    System.out.println(m);
    System.out.println(n);
    System.out.println(o);
    System.out.println(p);
    System.out.println(q);
    System.out.println(r);
    System.out.println(s);
    System.out.println(t);
    System.out.println(u);
    System.out.println(v);
    System.out.println(w);
    System.out.println(x);

	}
}
